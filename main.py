"""
This is the main file which is used to launch the game.
The main() function uses the Mygame class to launch the game.
"""
# --- Imports ---
import arcade
import Mygame

# -- Global Variables -- 
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Video Game"
BACKGROUND_COLOR = arcade.color.SKY_BLUE


def main():
    """ Main method """
    # Creates an instance of the game using the Mygame class.
    game = Mygame.MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    # Sets up the game using the setup() function from the Mygame class.
    game.setup()
    # Runs the game. This code comes from the Arcade parent class.
    arcade.run()


if __name__ == "__main__":
    main()



