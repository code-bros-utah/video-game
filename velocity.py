"""
File: velocity.py
Author: Jaden Mounteer
Purpose: This class represents the velocity of an object. Uses encapsulation (getters and setters) to
control the velocity of different objects.
"""

class Velocity():
    """
    Represents the velocity of an object.
    """
    def __init__(self):
        """
        Initiates the attributes of the Velocity class.
        """
        # Initiates the _dx and _dy attributes.
        self_dx = 0
        self_dy = 0

    def get_dx(self):
        """
        Gets _dx.
        :return: _dx
        """
        return self._dx

    def set_dx(self, dx):
        """
        Sets_dx using logic.
        :return: None
        """
        self._dx = dx

    def get_dy(self):
        """
        Gets _dy.
        :return: _dy
        """
        return self._dy

    def set_dy(self, dy):
        """
        Sets_dy using logic.
        :return: None
        """
        self._dy = dy

    # Creates the properties for the dx and dy attributes.
    x = property(get_dx, set_dx)
    y = property(get_dy, set_dy)