"""
File: point.py
Author: Jaden Mounteer
Purpose: This class represents a point on the screen. Uses encapsulation (getters
and setters) to make sure the objects in the game are where they need to be).
"""

class Point():
    """
    Represents a point on the screen.
    """
    def __init__(self):
        """
        Initiates the _x and _y attributes of Point.
        :return: None
        """
        self._x = 0
        self._y = 0

    def get_x(self):
        """
        Gets _x.
        :return: _x
        """
        return self._x

    def set_x(self, x):
        """
        Sets_x using logic.
        :return: None
        """
        self._x = x

    def get_y(self):
        """
        Gets _y.
        :return: _y
        """
        return self._y

    def set_y(self, y):
        """
        Sets_y using logic.
        :return: None
        """
        self._y = y

    # Creates the properties for the x and y attributes.
    x = property(get_x, set_x)
    y = property(get_y, set_y)




